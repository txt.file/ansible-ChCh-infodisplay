<!-- SPDX-License-Identifier: CC-BY-NC-ND-4.0 -->
<!-- SPDX-FileCopyrightText: 2021, Vieno Hakkerinen <vieno@hakkerinen.eu> -->

# ansible script to create a ChaosChemnitz infodisplay

This git repository contains ansible scripts to configure a Raspberry Pi as ChaosChemnitz infodisplay.

## quick start guide

* put Raspberry Pi OS (lite) on SD card
* mount SD card (both / and /boot)
* add file named "ssh" to mounted /boot
* add your ssh key to mounted /root/.ssh/authorized_keys
* unmount SD card, insert SD card into Raspberry Pi, boot Raspberry Pi
* run "ansible-playbook initial.yml -i inventory.factory"
* wait until Raspberry Pi has rebooted
* run "ansible-playbook infodisplay.yml"

## license

It is licensed as Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International.
